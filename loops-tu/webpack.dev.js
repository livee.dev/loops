const path = require('path')
const webpack = require('webpack')
const Merge = require('webpack-merge')
const CommonConfig = require('./webpack.common')

module.exports = Merge(CommonConfig, {
    devtool: 'eval-source-map',
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].js'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ],
    devServer: {
        hot: true,
        historyApiFallback: true,
        proxy: {
            "/api": "http://localhost:3001"
        }
    }
})