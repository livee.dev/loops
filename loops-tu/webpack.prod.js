const path = require('path')
const webpack = require('webpack')
const Merge = require('webpack-merge')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const CommonConfig = require('./webpack.common')

module.exports = Merge(CommonConfig, {
    mode: 'production',
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].js'
    },
    plugins: [
        new CleanWebpackPlugin(['dist'])
    ]
})
