import React from 'react'
import Login from './Login'
import styles from './Root.scss'

function getMsg() {
    return "hi My Friend"
}

export default () => (
    <div>
        <h1>{getMsg()}</h1>
        <div className={styles.hello}>Hello They</div>
        <Login/>
    </div>
)