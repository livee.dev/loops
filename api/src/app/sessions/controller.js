import Users from '../users/model'
import UsersController from '../users/controller'
import SessionsSerializer from './serializer'
import bcrypt from 'bcryptjs'

const SessionController = {
    async create(req, res) {
        const { email, password } = req.body

        const user = await Users.find({email: email})

        // console.log(user)

        const checkedPassword = bcrypt.compareSync(password, user[0].password)

        // console.log(checkedPassword)
        if(user.length == 1) {
            if((email == user.email)||(checkedPassword)) {
                res
                    .header('Authorization', `Bearer ${UsersController.getToken(user[0])}`)
                    .status(201)
                    .json({err: {msg: ''}, data: SessionsSerializer.for('create', user[0])})
            } else {
                res
                    .status(401)
                    .json({err: {msg: 'Invalid credentials.'}, data:{} })
            }
        } else {
            res
                .status(401)
                .json({err: {msg: 'Can not Find Email of User.'}, data:{} })
        }
    }
}

export default SessionController