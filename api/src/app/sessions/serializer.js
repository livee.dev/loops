import Serializer from '../serializer'

const SessionsSerializer = {
    ...Serializer,

    create(user) {
        // console.log("Serializer"+(typeof user));
        const {_id, email, author, status} = user

        return {_id, email, author, status}
    }
}

export default SessionsSerializer