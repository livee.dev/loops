import controller from './controller'

export function setup(router) {
    router
        .get('/', controller.getAll)
        .get('/page-query', controller.getPaging)
        .get('/:id', controller.get)
        .post('/', controller.create)
}