import mongoose from 'mongoose';

const userSchema = mongoose.Schema({
    email: { type: String, required: true},
    password: { type: String, default: ''},
    status: {type: Number, default: 1},
    author: {type: String, default: 'user'},
    imageURL: { type: String, default: ''},
    firstName: { type: String, default: ''},
    lastName: { type: String, default: ''},
    gender: { type: String, default: ''},
    birthday: { type: Date, default: '' },
    tel: { type: Number, default: '' },
    userRegister: { type: Date, default: Date.now()},
    userRegisterExpire: { type: Date, default: ''},
    facebook: { type: String, default: ''},
    tokens: Array,

    lineId: { type: String, default: ''},
    facebookId: { type: String, default: ''},
    googleId: { type: String, default: ''},

    passwordResetToken: {type: String, default: ''},
    passwordResetExpires: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Users', userSchema);