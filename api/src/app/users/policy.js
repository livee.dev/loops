import Policy from '../policy'

const UsersPolicy = {
    ...Policy,

    getAll(user) {
        return !!user
    }

}

export default UsersPolicy