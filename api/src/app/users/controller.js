import Users from './model'
import UsersSerializer from './serializer'
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import config from '../../config'
import UsersPolicy from './policy'

const UsersController = {
    getAll(req, res) {
        if(UsersPolicy.for('getAll', req.user)) {
            Users.find({}, (err, users) => {
                if (err) res.json({msg: err, data: []})
                if (users.length > 0) {
                    res.status(200).json({
                        msg: "",
                        data: UsersSerializer.for('getAll', users)
                    })
                } else {
                    res.status(404).json({msg: "No Data", data: {}})
                }
            })
        } else {
            res.status(401).json({msg: "You are not allowed to access.", data: {}})
        }
    },
    async getPaging(req, res) {
        const page = parseInt(req.query.page)
        const perPage = parseInt("5")
        console.log(page)
        const users = await Users.find({}).skip((page - 1)*perPage).limit(perPage).sort({userRegister: 'desc'})
        if (users) {
            res.status(200).json({err: {msg: ""}, data: UsersSerializer.for('getPaging', users)})
        } else {
            res.status(404).json({err: {msg: "User Not Found"}, data: {}})
        }

    },
    async get(req, res) {
        const id = req.params.id;
        const user = await Users.findById(id)
        if (user) {
            res.status(200).json({msg: "", data: UsersSerializer.for('get', user)})
        } else {
            res.status(404).json({msg: "User's ID is not Found", data: {}})
        }
    },
    create(req, res) {
        const email = req.body.email
        const password = req.body.password
        const firstName = req.body.firstName
        const lastName = req.body.lastName
        const author = req.body.author
        const status = req.body.status

        if (email || password) {
            bcrypt.hash(password, 12, async (err, hash) => {
                const newUsers = new Users({
                    email: email,
                    password: hash,
                    firstName: firstName,
                    lastName: lastName,
                    author: author,
                    status: status
                })

                let findByEmail = await Users.find({email: email})

                console.log(findByEmail.length)

                if (findByEmail.length > 0) {
                    res.status(400).json({err: {msg: "Email is complicated."}, data: {}})
                } else {
                    newUsers.save((err, user) => {
                        if (err) res.status(400).json({err: {msg: "Register is Failed."}, data: {}})

                        if (newUsers) {
                            res
                                .header('Authorization', `Bearer ${UsersController.getToken(user)}`)
                                .status(201)
                                .json({err: {msg: "Register is Completed"}, data: user})
                        } else {
                            res.status(400).json({err: {msg: "Register is Failed."}, data: {}})
                        }
                    })
                }
            })
        } else {
            res.json({
                err: {msg: "Email Or Password is not found."}
            })
        }
    },

    getToken(user) {
        return jwt.sign({ sub: user._id}, 'secret', { expiresIn: '1h'})
    }
}

export default UsersController