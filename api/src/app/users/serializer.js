import Serializer from '../serializer'

const UsersSerializer = {
    ...Serializer,

    get(user) {
        return this.serialize(user)
    },

    getAll(users) {
        return users.map(user => this.serialize(user))
    },

    getPaging(users) {
        return users.map(user => this.serialize(user))
    },

    serialize(user) {
        const {_id, email, author, status} = user

        return {_id, email, author, status}
    }
}

export default UsersSerializer