import express from 'express'
import bodyParser from 'body-parser'
import fs from 'fs'
import cors from 'cors'
import mongoose from 'mongoose'
import morgan from 'morgan';
import winston from 'winston'
import config from './config'
import auth from './middlewares/auth'

export function setup() {

    mongoose.Promise = global.Promise;
    mongoose.connect('mongodb://admin:thelittlepony@cluster0-shard-00-00-qptgu.mongodb.net:27017,cluster0-shard-00-01-qptgu.mongodb.net:27017,cluster0-shard-00-02-qptgu.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin');

    const app = express();

    const PORT = config.port;

    function setupRoutes(app) {

        const APP_DIR = `${__dirname}/app`;
        const features = fs.readdirSync(APP_DIR).filter(
            file => fs.statSync(`${APP_DIR}/${file}`).isDirectory()
        );

        features.forEach(feature => {
            const router = express.Router();
            const routes = require(`${APP_DIR}/${feature}/routes.js`);

            routes.setup(router);
            app.use(`/api/${feature}`, router)
        })

    }

    app.use(auth)
    app.use(bodyParser.urlencoded({extend: true}));
    app.use(bodyParser.json());
    app.use(cors());
    setupRoutes(app);

    app.listen(PORT, () =>
        console.log("App listening on PORT:" + PORT)
    )
}