require('dotenv').config()

export default  {
    port: process.env.PORT || 3001,
    secretKey: process.env.SECRET_KEY
}