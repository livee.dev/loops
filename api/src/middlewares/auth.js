import jwt from 'jsonwebtoken'
import config from '../config'
import Users from '../app/users/model'

export default function (req, res, next) {
    const authHeader = req.header('Authorization')

    if(!authHeader) return next()

    const accessToken = authHeader.match(/Bearer (.*)/)[1]

    jwt.verify(accessToken, config.secretKey, async (err, decoded) => {
        // console.log(err)
        // console.log(decoded)
        if(err) return next()
        const findUser = await Users.findById(decoded.sub)
        req.user = findUser
        next()
    })
}